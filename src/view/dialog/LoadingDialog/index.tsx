import React from 'react';
import {Modal, StyleSheet, View, ActivityIndicator} from 'react-native';

type Props = {
  isShow: boolean;
};

const LoadingDialog: React.FC<Props> = ({isShow}) => (
  <Modal animationType="fade" transparent={true} visible={isShow}>
    <View style={styles.centeredView}>
      <ActivityIndicator size="large" color={'#2F6BFF'} />
    </View>
  </Modal>
);

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(230,230,230,0.5)',
  },
});

export default LoadingDialog;
