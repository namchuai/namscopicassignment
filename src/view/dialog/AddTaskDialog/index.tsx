import React, {useState} from 'react';
import {
  Button,
  Dimensions,
  Modal,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';

type Props = {
  visible: boolean;
  onSubmitTaskPress: (taskName: string) => void;
  onClosePress: () => void;
};

const {width} = Dimensions.get('window');

const AddTaskDialog: React.FC<Props> = ({
  visible,
  onSubmitTaskPress,
  onClosePress,
}) => {
  const [taskName, setTaskName] = useState('');

  const maxCharAvailable = 40;

  const onDialogClosePress = () => {
    onClosePress();
    setTaskName('');
  };

  const onAddTaskPress = () => {
    if (taskName.trim().length === 0) {
      return;
    }
    onSubmitTaskPress(taskName);
    setTaskName('');
  };

  return (
    <Modal
      animationType="fade"
      transparent
      visible={visible}
      presentationStyle="overFullScreen">
      <View style={styles.viewWrapper}>
        <View style={styles.modalView}>
          <Text style={styles.dialogHeader}>Add new task</Text>
          <TextInput
            autoCapitalize="none"
            placeholder="Enter task name.."
            value={taskName}
            style={styles.textInput}
            onChangeText={value => {
              if (value.length <= maxCharAvailable) {
                setTaskName(value);
              }
            }}
          />
          <Text style={styles.wordCount}>
            {taskName.length}/{maxCharAvailable}
          </Text>
          <View style={styles.bottomControl}>
            <Button title="Close" onPress={onDialogClosePress} />
            <Button title="Add Task" onPress={onAddTaskPress} />
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalView: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    elevation: 5,
    transform: [{translateX: -(width * 0.4)}, {translateY: -90}],
    width: width * 0.8,
    backgroundColor: '#fff',
    borderRadius: 7,
    paddingHorizontal: 24,
    paddingBottom: 12,
  },
  wordCount: {
    color: '#00D3D3',
    textAlign: 'right',
  },
  dialogHeader: {
    marginTop: 24,
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 12,
  },
  viewWrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.2)',
  },
  textInput: {
    width: '100%',
    borderRadius: 5,
    paddingVertical: 8,
    paddingHorizontal: 16,
    borderColor: 'rgba(0, 0, 0, 0.2)',
    height: 42,
    borderWidth: 1,
    marginBottom: 8,
  },
  bottomControl: {
    marginTop: 12,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
});

export default AddTaskDialog;
