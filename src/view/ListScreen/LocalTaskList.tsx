import React from 'react';
import {FlatList} from 'react-native';
import HorizontalDivider from '../components/HorizontalDivider';
import TaskTile from '../components/TaskTile';
import {Task} from '../../model/Task';

type Props = {
  taskList: Task[];
  onRemoveTask: (taskId: string) => void;
};

const LocalTaskList: React.FC<Props> = ({taskList, onRemoveTask}) => (
  <FlatList
    ItemSeparatorComponent={() => <HorizontalDivider />}
    keyExtractor={item => item.id}
    data={taskList}
    renderItem={({item}) => (
      <TaskTile task={item} onDeteleTask={onRemoveTask} />
    )}
  />
);

export default LocalTaskList;
