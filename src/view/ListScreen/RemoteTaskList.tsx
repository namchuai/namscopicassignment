import React, {useEffect, useState} from 'react';
import {FlatList} from 'react-native';
import {useProfile} from '../../context/ProfileContext';
import HorizontalDivider from '../components/HorizontalDivider';
import TaskTile from '../components/TaskTile';
import firestore from '@react-native-firebase/firestore';
import {Task} from '../../model/Task';

const RemoteTaskList = () => {
  const {profile} = useProfile();
  const [taskList, setTaskList] = useState<Array<Task>>([]);

  useEffect(() => {
    const subscriber = firestore()
      .collection('tasks')
      .where('createdBy', '==', profile!.uid)
      .orderBy('createdAt', 'desc')
      .onSnapshot(documentSnapshot => {
        const tasks: Task[] = [];
        documentSnapshot.docs.map(doc => {
          tasks.push({
            id: doc.id,
            title: doc.data().title,
            createdAt: doc.data().createdAt,
            createdBy: doc.data().createdBy,
          });
        });
        setTaskList(tasks);
      });

    return () => subscriber();
  }, [profile]);

  const removeTask = (taskId: string) =>
    firestore().collection('tasks').doc(taskId).delete();

  return (
    <FlatList
      ItemSeparatorComponent={() => <HorizontalDivider />}
      keyExtractor={item => item.id}
      data={taskList}
      renderItem={({item}) => (
        <TaskTile task={item} onDeteleTask={removeTask} />
      )}
    />
  );
};

export default RemoteTaskList;
