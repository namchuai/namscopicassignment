import React, {useEffect, useState} from 'react';
import {StyleSheet, Switch, View} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {useNavigation} from '@react-navigation/core';
import AddTaskDialog from '../dialog/AddTaskDialog';
import HeaderTextButton from '../components/HeaderTextButton';
import firestore from '@react-native-firebase/firestore';
import {useProfile} from '../../context/ProfileContext';
import RemoteTaskList from './RemoteTaskList';
import LocalTaskList from './LocalTaskList';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Task} from '../../model/Task';
import {FloatingAction} from 'react-native-floating-action';

const ListScreen = () => {
  const {profile} = useProfile();
  const insets = useSafeAreaInsets();
  const navigation = useNavigation();
  const [usingFirestore, setUsingFirestore] = useState(false);

  const [addTaskVisible, setAddTaskVisible] = useState(false);
  const [taskList, setTaskList] = useState<Array<Task>>([]);

  useEffect(() => {
    async function getLocalTaskList() {
      try {
        const value = await AsyncStorage.getItem('@taskList');
        const localTaskList: Array<Task> = [];
        if (value != null) {
          const jsonData = JSON.parse(value);
          for (var i = 0; i < jsonData.length; i++) {
            localTaskList.push({
              id: jsonData[i].id,
              title: jsonData[i].title,
              createdBy: jsonData[i].createdBy,
              createdAt: jsonData[i].createdAt,
            });
          }
        }
        setTaskList(localTaskList);
      } catch (e) {
        console.log(e);
      }
    }

    getLocalTaskList();
  }, []);

  useEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <HeaderTextButton
          title="Profile"
          onPress={() => navigation.navigate('ProfileScreen')}
        />
      ),
    });
  }, []);

  const addLocalTask = async (taskName: string) => {
    try {
      const newTaskList = [...taskList];
      const newTask: Task = {
        id: taskList.length.toString(),
        title: taskName,
        createdBy: profile!.uid,
        createdAt: Date.now(),
      };
      newTaskList.splice(0, 0, newTask);
      await storeLocalTask(newTaskList);
      setTaskList(newTaskList);
    } catch (e) {
      console.log(e);
    }
  };

  const storeLocalTask = async (tasks: Array<Task>) => {
    await AsyncStorage.setItem('@taskList', JSON.stringify(tasks));
  };

  const addRemoteTask = (taskName: string) => {
    firestore().collection('tasks').add({
      title: taskName,
      createdBy: profile!.uid,
      createdAt: Date.now(),
    });
  };

  const onSubmitTaskPress = (taskName: string) => {
    if (usingFirestore) {
      addRemoteTask(taskName);
    } else {
      addLocalTask(taskName);
    }
    setAddTaskVisible(false);
  };

  const onRemoveLocalTask = async (taskId: string) => {
    const newTaskList = [...taskList];
    let removeIndex = -1;
    for (var i = 0; i < newTaskList.length; i++) {
      if (newTaskList[i].id === taskId) {
        removeIndex = i;
        break;
      }
    }
    newTaskList.splice(removeIndex, 1);
    await storeLocalTask(newTaskList);
    setTaskList(newTaskList);
  };

  const onAddTaskPress = () => setAddTaskVisible(true);

  const onAddTaskClosePress = () => setAddTaskVisible(false);

  const toggleSwitch = () => setUsingFirestore(previousState => !previousState);

  const actions = [
    {
      text: 'Add Task',
      name: 'add_task',
      position: 1,
    },
  ];

  return (
    <View
      style={[styles.container, {paddingBottom: Math.max(insets.bottom, 16)}]}>
      <AddTaskDialog
        visible={addTaskVisible}
        onClosePress={onAddTaskClosePress}
        onSubmitTaskPress={onSubmitTaskPress}
      />
      <Switch
        style={styles.onlineSwitch}
        onValueChange={toggleSwitch}
        value={usingFirestore}
      />
      {usingFirestore ? (
        <RemoteTaskList />
      ) : (
        <LocalTaskList taskList={taskList} onRemoveTask={onRemoveLocalTask} />
      )}
      <FloatingAction
        color="#DE555D"
        actions={actions}
        onPressItem={() => {
          onAddTaskPress();
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  onlineSwitch: {
    margin: 24,
    alignSelf: 'center',
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
  addTaskButton: {
    marginHorizontal: 32,
  },
});

export default ListScreen;
