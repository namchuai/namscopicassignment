import {useNavigation} from '@react-navigation/core';
import React, {useCallback, useState} from 'react';
import {Alert, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import FilledButton from '../components/FilledButton';
import InputWithHeader from '../components/InputWithHeader';
import auth from '@react-native-firebase/auth';
import LoadingDialog from '../dialog/LoadingDialog';

const LoginScreen = () => {
  const navigation = useNavigation();
  const insets = useSafeAreaInsets();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loadingVisible, setLoadingVisible] = useState(false);

  const onSignInPress = useCallback(async () => {
    setLoadingVisible(true);
    await auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => setLoadingVisible(false))
      .catch(error => {
        setLoadingVisible(false);
        showAlert(error.message);
      });
  }, [email, password]);

  const showAlert = (message: string) =>
    Alert.alert('Login failed', message, [{text: 'OK', onPress: () => {}}]);

  const onSignUpPress = useCallback(
    () => navigation.navigate('SignUpScreen'),
    [],
  );

  return (
    <View style={styles.container}>
      <LoadingDialog isShow={loadingVisible} />
      <View
        style={[
          styles.expandContainer,
          {paddingTop: Math.max(insets.top, 16)},
        ]}>
        <Text style={styles.scopicTitle}>Scopic</Text>
        <Text style={styles.signInTitle}>Sign In</Text>
        <InputWithHeader title="Email" text={email} onTextChange={setEmail} />
        <InputWithHeader
          title="Password"
          text={password}
          obscuredText={true}
          onTextChange={setPassword}
        />
        <FilledButton title="Sign In" onPress={onSignInPress} />
      </View>
      <TouchableOpacity
        onPress={onSignUpPress}
        style={{
          paddingBottom: Math.max(insets.bottom, 16),
        }}>
        <View style={styles.signUpWrapper}>
          <Text style={styles.title}>Sign Up</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingHorizontal: 32,
  },
  scopicTitle: {
    margin: 52,
    alignSelf: 'center',
    fontWeight: 'bold',
    fontSize: 32,
  },
  signInTitle: {
    marginBottom: 52,
    fontWeight: 'bold',
    fontSize: 24,
  },
  expandContainer: {
    flex: 1,
  },
  title: {
    color: '#DE555D',
    fontWeight: 'bold',
  },
  signUpWrapper: {
    alignSelf: 'flex-end',
  },
});

export default LoginScreen;
