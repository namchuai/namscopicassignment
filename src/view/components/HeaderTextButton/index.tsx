import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';

type Props = {
  title: string;
  onPress?: () => void;
};

const HeaderTextButton: React.FC<Props> = ({title, onPress}) => (
  <TouchableOpacity onPress={onPress}>
    <Text style={styles.title}>{title}</Text>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  title: {
    color: '#DE555D',
    fontWeight: 'bold',
    fontSize: 16,
  },
});

export default HeaderTextButton;
