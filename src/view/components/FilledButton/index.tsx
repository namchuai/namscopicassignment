import React from 'react';
import {
  StyleSheet,
  Text,
  TextStyle,
  TouchableOpacity,
  ViewStyle,
} from 'react-native';

type Props = {
  title: string;
  style?: ViewStyle;
  textStyle?: TextStyle;
  onPress: () => void;
};

const FilledButton: React.FC<Props> = ({title, style, onPress}) => (
  <TouchableOpacity style={[style, styles.container]} onPress={onPress}>
    <Text style={[styles.titleStyle]}>{title}</Text>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#DE555D',
    borderRadius: 8,
    height: 52,
    alignItems: 'center',
    justifyContent: 'center',
  },
  titleStyle: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default FilledButton;
