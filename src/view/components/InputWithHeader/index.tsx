import React from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native';

type Props = {
  title: string;
  text: string;
  obscuredText?: boolean;
  onTextChange: (text: string) => void;
};

const InputWithHeader: React.FC<Props> = ({
  title,
  text,
  obscuredText = false,
  onTextChange,
}) => (
  <View>
    <Text style={styles.title}>{title}</Text>
    <View style={styles.inputContainer}>
      <TextInput
        autoCapitalize={'none'}
        onChangeText={onTextChange}
        value={text}
        secureTextEntry={obscuredText}
      />
    </View>
  </View>
);

const styles = StyleSheet.create({
  title: {
    color: '#DE555D',
    fontWeight: 'bold',
  },
  inputContainer: {
    justifyContent: 'center',
    height: 42,
    marginTop: 8,
    marginBottom: 24,
    borderBottomWidth: 1,
    borderBottomColor: '#D3D3D3',
  },
});

export default InputWithHeader;
