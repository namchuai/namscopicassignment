import React from 'react';
import {StyleSheet, View} from 'react-native';

const HorizontalDivider = () => <View style={styles.container} />;

const styles = StyleSheet.create({
  container: {
    height: 1,
    backgroundColor: '#D3D3D3',
    marginLeft: 12,
  },
});

export default React.memo(HorizontalDivider);
