import {useNavigation} from '@react-navigation/core';
import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';

const SignInTextButton = () => {
  const navigation = useNavigation();

  const onSignInPress = () => {
    navigation.navigate('LoginScreen');
  };

  return (
    <View style={styles.container}>
      <Text>Have an account?</Text>
      <TouchableOpacity onPress={onSignInPress}>
        <Text style={styles.signInButton}>Sign In</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 24,
    flexDirection: 'row',
    alignSelf: 'center',
  },
  signInButton: {
    paddingLeft: 12,
    color: '#DE555D',
    fontWeight: 'bold',
  },
});

export default React.memo(SignInTextButton);
