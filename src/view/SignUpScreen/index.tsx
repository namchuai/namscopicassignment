import React, {useCallback, useState} from 'react';
import {Alert, StyleSheet, Text, View} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import FilledButton from '../components/FilledButton';
import InputWithHeader from '../components/InputWithHeader';
import SignInTextButton from '../components/SignInTextButton';
import auth from '@react-native-firebase/auth';
import LoadingDialog from '../dialog/LoadingDialog';

const SignUpScreen = () => {
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [loadingVisible, setLoadingVisible] = useState(false);

  const onSignUpPress = useCallback(async () => {
    setLoadingVisible(true);
    await auth()
      .createUserWithEmailAndPassword(email, password)
      .then(() => setLoadingVisible(false))
      .catch(error => {
        setLoadingVisible(false);
        if (error.code === 'auth/email-already-in-use') {
          showAlert('That email address is already in use!');
          return;
        }

        if (error.code === 'auth/invalid-email') {
          showAlert('That email address is invalid!');
          return;
        }

        showAlert(error);
      });
  }, [email, password]);

  const showAlert = (message: string) =>
    Alert.alert('Sign up failed', message, [{text: 'OK', onPress: () => {}}]);

  return (
    <View style={styles.container}>
      <LoadingDialog isShow={loadingVisible} />
      <SafeAreaView>
        <Text style={styles.signUpTitle}>Sign Up</Text>
        <InputWithHeader title="Email" text={email} onTextChange={setEmail} />
        <InputWithHeader
          title="Password"
          text={password}
          obscuredText={true}
          onTextChange={setPassword}
        />
        <FilledButton title="Sign Up" onPress={onSignUpPress} />
        <SignInTextButton />
      </SafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingHorizontal: 32,
  },
  signUpTitle: {
    marginTop: 52,
    marginBottom: 52,
    fontWeight: 'bold',
    fontSize: 24,
  },
});

export default SignUpScreen;
