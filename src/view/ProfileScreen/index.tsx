import React, {useCallback, useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {useProfile} from '../../context/ProfileContext';
import FilledButton from '../components/FilledButton';
import auth from '@react-native-firebase/auth';
import {useNavigation} from '@react-navigation/native';
import HeaderTextButton from '../components/HeaderTextButton';
import AsyncStorage from '@react-native-async-storage/async-storage';

const ProfileScreen = () => {
  const insets = useSafeAreaInsets();
  const navigation = useNavigation();
  const {profile} = useProfile();

  useEffect(() => {
    navigation.setOptions({
      headerLeft: () => {
        return (
          <HeaderTextButton title="Back" onPress={() => navigation.goBack()} />
        );
      },
    });
  }, []);
  const onLogOutPress = useCallback(async () => {
    await AsyncStorage.removeItem('@taskList');
    await AsyncStorage.removeItem('@userDetail');
    await auth().signOut();
  }, []);

  return (
    <View
      style={[styles.container, {paddingBottom: Math.max(insets.bottom, 16)}]}>
      <View style={styles.expandContainer}>
        <Text style={styles.title}>{profile?.email}</Text>
      </View>
      <FilledButton
        title="Log Out"
        onPress={onLogOutPress}
        style={styles.logOutButton}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  expandContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logOutButton: {
    marginHorizontal: 32,
  },
});

export default ProfileScreen;
