import AsyncStorage from '@react-native-async-storage/async-storage';
import {useNavigation} from '@react-navigation/core';
import React, {useCallback} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import FilledButton from '../components/FilledButton';

const WelcomeScreen = () => {
  const navigation = useNavigation();
  const insets = useSafeAreaInsets();

  const onListPress = useCallback(() => {
    AsyncStorage.setItem(
      '@userDetail',
      JSON.stringify({finishWelcome: true}),
    ).then(() => {
      navigation.navigate('ListScreen');
    });
  }, []);

  return (
    <View
      style={[styles.container, {paddingBottom: Math.max(insets.bottom, 16)}]}>
      <View style={styles.centerContent}>
        <Text>Hi there! Nice to see you.</Text>
      </View>
      <FilledButton title="List" onPress={onListPress} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingHorizontal: 32,
  },
  centerContent: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default WelcomeScreen;
