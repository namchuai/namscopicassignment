export type Task = {
  id: string;
  title: string;
  createdBy: string;
  createdAt: number;
};
