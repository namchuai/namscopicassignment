export type UserProfile = {
  uid: string;
  email: string;
};
