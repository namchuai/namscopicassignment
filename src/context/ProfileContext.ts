import {createContext, useContext} from 'react';
import {UserProfile} from '../model/UserProfile';

type ProfileContextType = {
  profile?: UserProfile;
  setProfile: (profile?: UserProfile) => void;
};

export const ProfileContext = createContext<ProfileContextType>({
  profile: undefined,
  setProfile: () => {},
});

export const useProfile = () => useContext(ProfileContext);
