import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React, {useEffect, useState} from 'react';
import ListScreen from './src/view/ListScreen';
import LoginScreen from './src/view/LoginScreen';
import ProfileScreen from './src/view/ProfileScreen';
import SignUpScreen from './src/view/SignUpScreen';
import WelcomeScreen from './src/view/WelcomeScreen';
import auth from '@react-native-firebase/auth';
import {UserProfile} from './src/model/UserProfile';
import {ProfileContext} from './src/context/ProfileContext';
import HeaderTextButton from './src/view/components/HeaderTextButton';
import {GestureHandlerRootView} from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {StyleSheet} from 'react-native';

const Stack = createNativeStackNavigator();

const App = () => {
  const [initializing, setInitializing] = useState(true);
  const [profile, setProfile] = useState<UserProfile>();
  const [finishedWelcome, setFinishedWelcome] = useState(false);

  const onAuthStateChanged = (user: any) => {
    if (user == null) {
      setProfile(undefined);
      if (initializing) {
        setInitializing(false);
      }
    } else {
      AsyncStorage.getItem('@userDetail')
        .then(value => {
          if (value != null) {
            let jsonData = JSON.parse(value);
            if (jsonData.finishWelcome) {
              setFinishedWelcome(true);
              setProfile({uid: user.uid, email: user.email});
              if (initializing) {
                setInitializing(false);
              }
            } else {
              setFinishedWelcome(false);
              setProfile({uid: user.uid, email: user.email});
              if (initializing) {
                setInitializing(false);
              }
            }
          } else {
            setFinishedWelcome(false);
            setProfile({uid: user.uid, email: user.email});
            if (initializing) {
              setInitializing(false);
            }
          }
        })
        .catch(err => {
          console.log(err);
        });
    }
  };

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber;
  }, []);

  if (initializing) {
    return null;
  }

  if (!profile) {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="LoginScreen">
          <Stack.Screen
            name="LoginScreen"
            component={LoginScreen}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="SignUpScreen"
            component={SignUpScreen}
            options={{
              headerShown: false,
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }

  return (
    <ProfileContext.Provider value={{profile, setProfile}}>
      <GestureHandlerRootView style={styles.container}>
        <NavigationContainer>
          <Stack.Navigator
            initialRouteName={finishedWelcome ? 'ListScreen' : 'WelcomeScreen'}>
            <Stack.Screen
              name="WelcomeScreen"
              component={WelcomeScreen}
              options={{
                title: 'Welcome',
              }}
            />
            <Stack.Screen
              name="ProfileScreen"
              component={ProfileScreen}
              options={{
                title: 'Profile',
                headerLeft: () => {
                  return <HeaderTextButton title="Back" />;
                },
              }}
            />
            <Stack.Screen
              name="ListScreen"
              component={ListScreen}
              options={{
                title: 'List',
              }}
            />
          </Stack.Navigator>
        </NavigationContainer>
      </GestureHandlerRootView>
    </ProfileContext.Provider>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default App;
